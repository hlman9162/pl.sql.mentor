--pl/sql syntax

DECLARE
  --DEYISENLERI TANIDIRIQ
  --BU HISSENIN YAZMAQ MUTLEQ DEYIL
BEGIN
  --BURADA ISLENILECEK KODLAR YAZILIR
  --BEGIN ILE END IN ARASINDA MUTLEQ SEKILDE EMR YAZILAMLIDIRI
  --BOS BURAXILMAZI DUZGUN DEYIL
  EXCEPTION
    --HER HANSI BIR XETA YARANAN ZAMAN FEALIYYETE BASLAYIR
    --YAZMAQ ZERURI DEYIL
END;


--DEYISEN TANIMLAMA
DECLARE
  --DEYISENIN ADI  DEYISENIN TIPI;
  --DEYISENIN ADI  DEYISENIN TIPI := DEYERI;
BEGIN
  --PROSES YAZMAQ LAZIMDI EN AZ BIRDENE
END;

SET SERVEROUTPUT ON

 DECLARE 
  v_first_name varchar2(30);
   v_last_name  varchar2(30);
    v_age        number;
     v_birth_day  date;
      v_length     number(3,2):= 1.75;
       v_weight     float:=0;
        v_salary     number(5,2);
         v_tax_amount number:=0;
 BEGIN
  v_first_name:='Laman';
   v_last_name:='Huseynli';
    v_age:=25;
     v_birth_day:=to_date('18.07.2004','dd,MM,yyyy');
      v_weight:=57.20;
       v_salary:=800;
        v_tax_amount:=round(v_salary*15/100,2);
 
dbms_output.put_line('Istifadecini adi:'||v_first_name||' Istifadecini soyadi:'||v_last_name);
dbms_output.put_line('Vergi:'||v_tax_amount);
END; 
 
 
--
--table yarat

select*from employees;
 DECLARE
  v_first_name  employees.first_name%type;
  v_last_name   employees.last_name%type;
  v_hire_date   employees.hire_date%type;
  v_salary      employees.salary%type;
  v_system_user varchar2(30);
  v_registration date;
 BEGIN
  --SELECT INTO --SELECT NETICESINDE GELEN NETICENI DEYISENLERE MENIMSETMEK UCUNDUR
 SELECT USER,sysdate  INTO V_SYSTEM_USER,V_REGISTRATION FROM DUAL;
 
 SELECT T.FIRST_NAME,T.LAST_NAME,T.HIRE_DATE,T.SALARY  
     INTO  V_FIRST_NAME,V_LAST_NAME,V_HIRE_DATE,V_SALARY
       FROM EMPLOYEES T  WHERE T.EMPLOYEE_ID=180;
       
 INSERT INTO EMPLOYEES(EMPLOYEE_ID,FIRST_NAME,LAST_NAME,HIRE_DATE,SALARY)
    VALUES (203,V_FIRST_NAME,V_LAST_NAME,V_HIRE_DATE,V_SALARY);
 
 DBMS_OUTPUT.PUT_LINE('Istifadeci adi:'||v_system_user||' Qeydiyyat vaxti:'||v_registration);

 END;
 
--If
--If Else
--If Elsif else

IF sert  THEN
  Sert dogru oldugu halda isleycek kod;
END IF;

IF sert THEN 
  sert duzgun oldugu halda isleyecek kod
 ELSE 
  sert duzgun olmadigi halda islenilecek kod
END IF;

IF sert1 THEN
  sert 1 duzgun oldugu halda isleyecek kod;
 ELSIF sert2  THEN
  sert2 duzgun oldugu hada isleyecek kod;
 ELSE
  sertlerin hec biri dogru olmadigi halda isleyeck kod;
END IF;


       


 