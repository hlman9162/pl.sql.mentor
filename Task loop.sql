**Basic Simple Loop**
    - Task: Write a simple loop that prints numbers from 1 to 5.
DECLARE
  v_counter NUMBER := 1;
BEGIN
  LOOP
    DBMS_OUTPUT.PUT_LINE(v_counter);
    v_counter := v_counter + 1;
    EXIT WHEN v_counter > 5;
  END LOOP;
END;


 **FOR Loop with Range**
    - Task: Use a FOR loop to print numbers from 1 to 10.
    BEGIN
  FOR v_num IN 1..10 LOOP
     DBMS_OUTPUT.PUT_LINE(v_num);
  END LOOP;
END;


 **Nested Loops**
    - Task: Create nested loops to print a pattern of numbers.
BEGIN
  FOR i IN 1..3 LOOP
      FOR j IN 1..3 LOOP
        DBMS_OUTPUT.PUT_LINE(i || ', ' || j);
     END LOOP;
  END LOOP;
END;


 **LOOP with EXIT WHEN**
    - Task: Create a loop that starts with a variable set to 10 and decrements it, printing out its value, until it's less than 5.
  DECLARE
   v_counter NUMBER := 10;
BEGIN
  LOOP
     DBMS_OUTPUT.PUT_LINE(v_counter);
     v_counter := v_counter - 1;
     IF v_counter < 5 THEN
        EXIT;
     END IF;
  END LOOP;
END;
   



