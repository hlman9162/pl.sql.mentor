Oracle SQL görünüşü adi cədvəl kimi fəaliyyət göstərən, lakin verilənlərin özünü saxlamayan virtual cədvəldir. Bunun əvəzinə, bir və ya bir neçə əsas cədvəldən məlumatları əldə edən sorğu təyin edir.

View dan istifadə etmək üçün bəzi ümumi səbəblər bunlardır:

Mürəkkəb sorğuları sadələşdirin: VIEW mürəkkəb SQL məntiqini istifadəçilərdən gizlədə bilər ki, bu da onların məlumatlara daxil olmasını asanlaşdırır.
Məlumat təhlükəsizliyi: VIEW əsas cədvəllərdə müəyyən sütun və ya sətirlərə girişi məhdudlaşdıra bilər.
Verilənləri xüsusi formatda təqdim edin: VIEW məlumatları istifadəçilər üçün daha mənalı şəkildə təqdim edə bilər.

Oracle SQL VIEW nu  necə yaratmaq olar

Siz Oracle SQL-də CREATE VIEW ifadəsindən istifadə edərək görünüş yarada bilərsiniz. Bəyanat görünüşün adını, onun ehtiva edəcəyi sütunları və əldə ediləcək məlumatları təyin edən SQL sorğusunu müəyyən edir.

Məsələn, aşağıdakı ifadə müştəri və sifariş cədvəllərindən müştəri adını, sifariş nömrəsini və sifariş tarixini göstərən customer_orders adlı VIEW yaradır:

CREATE OR REPLACE VIEW customer_orders AS
SELECT c.customer_name, o.order_number, o.order_date
FROM customers c
JOIN orders o ON c.customer_id = o.customer_id;

Siz adi cədvəl kimi SQL sorğularınızda VIEW dən istifadə edə bilərsiniz. Məsələn, aşağıdakı sorğu müştəri_sifarişləri VIEW dən bütün sifarişləri əldə edir:

SELECT * FROM customer_orders;

