PL/SQL'de exception handling xetalerin ve ya gozlenilmez durumlarin yönetilmesi ve işlenmesi ucun istifade edilir. PL/SQL'de istisnalar, programın normal gediatini pozan ve xususi bir veziyyet kimi ele alinilmasi prosesdir. Exception handling, bu hadisələrin idarə olunmasını və müvafiq cavabların verilməsini təmin edir.

PL/SQL'de Exception Novleri
PL/SQL'de iki ana nov exception vardır:

Predefined Exceptions (Evvelceden teyin edilmis):
Bu istisnalar Oracle tərəfindən əvvəlcədən müəyyən edilir və ümumi səhvlər üçün istifadə olunur. Bəzi ümumi standart istisnalar bunlardır:

NO_DATA_FOUND: Bir SELECT INTO ifadesi hecbir setir dondurmezse 
TOO_MANY_ROWS: Bir SELECT INTO ifadesi birden cox setir dondurerse 
ZERO_DIVIDE: Bir sayının sıfıra bölünmesi halinda yaranir.
INVALID_CURSOR: Etibarsiz bir cursor işlemi istifade edilmeye çalışıldığında 
ACCESS_INTO_NULL: Bir NULL koleksiyonuna girisi edildiyinde yaranir.


User-defined Exceptions (İstifadəçi tərəfindən müəyyən edilmiş istisnalar):

Bu istisnalar istisnaları idarə etmək üçün proqramçılar tərəfindən müəyyən edilir. İstifadəçi tərəfindən müəyyən edilmiş istisnalar Declare bölməsində müəyyən edilir və lazım olduqda RAISE ifadəsi ilə işə salınır.

Exception Handling Syntax
PL/SQL'de exception handling şu temel yapıya sahiptir:

BEGIN
   -- PL/SQL kodu
EXCEPTION
   WHEN exception_name1 THEN
      -- İstisna handling kodu
   WHEN exception_name2 THEN
      -- İstisna handling kodu
   WHEN OTHERS THEN
      -- Diğer tüm istisnalar için handling kodu
END;


Numuneler
Öntanımlı İstisna Kullanımı

DECLARE
   v_number NUMBER;
BEGIN
   v_number := 1 / 0;
EXCEPTION
   WHEN ZERO_DIVIDE THEN
      DBMS_OUTPUT.PUT_LINE('Sıfıra bölme hatası oluştu.');
END;


Kullanıcı Tanımlı İstisna Kullanımı
DECLARE
   e_custom_exception EXCEPTION;
BEGIN
   -- Özel bir durum kontrol ediliyor
   IF some_condition THEN
      RAISE e_custom_exception;
   END IF;
EXCEPTION
   WHEN e_custom_exception THEN
      DBMS_OUTPUT.PUT_LINE('Özel bir istisna yakalandı.');
END;

OTHERS Kullanımı
DECLARE
   v_number NUMBER;
BEGIN
   v_number := 1 / 0;
EXCEPTION
   WHEN ZERO_DIVIDE THEN
      DBMS_OUTPUT.PUT_LINE('Sıfıra bölme hatası.');
   WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Başka bir hata oluştu: ' || SQLERRM);
END;

Özet
PL/SQL'de istisna yönetimi, programların daha sağlam ve hatalara karşı dayanıklı olmasını sağlar. Ön tanımlı istisnalar yaygın hataları ele alırken, kullanıcı tanımlı istisnalar özel durumlar için kullanılır. EXCEPTION bloğu içinde WHEN ve OTHERS ifadeleri kullanılarak bu istisnalar yönetilir.

PL/SQL exception handling konusunda kendinizi geliştirmek için çeşitli görevler yapabilirsiniz. İşte birkaç örnek görev:

### Görev 1: Basit Exception Handling

Bir PL/SQL bloğu yazın ve `NO_DATA_FOUND` istisnasını yakalayın. Örneğin, bir SELECT INTO ifadesi kullanarak belirli bir kaydı sorgulayın ve bu kayıt bulunamazsa uygun bir mesaj gösterin.

```plsql
DECLARE
   v_employee_name VARCHAR2(50);
BEGIN
   SELECT name INTO v_employee_name FROM employees WHERE employee_id = 9999;
EXCEPTION
   WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Kayıt bulunamadı.');
END;
```

### Görev 2: Kullanıcı Tanımlı İstisna

Kullanıcı tanımlı bir istisna oluşturun ve belirli bir koşul sağlandığında bu istisnayı tetikleyin. Örneğin, bir değişkenin değeri belirli bir eşiğin üzerinde olduğunda özel bir hata mesajı gösterin.

```plsql
DECLARE
   e_value_too_high EXCEPTION;
   v_value NUMBER := 101;
BEGIN
   IF v_value > 100 THEN
      RAISE e_value_too_high;
   END IF;
EXCEPTION
   WHEN e_value_too_high THEN
      DBMS_OUTPUT.PUT_LINE('Değer 100'den büyük.');
END;
```

### Görev 3: Birden Fazla İstisna Yönetimi

Birden fazla istisna durumu için bir PL/SQL bloğu oluşturun. Örneğin, sıfıra bölme hatasını ve diğer genel hataları yakalayın.

```plsql
DECLARE
   v_number NUMBER;
BEGIN
   v_number := 1 / 0;
EXCEPTION
   WHEN ZERO_DIVIDE THEN
      DBMS_OUTPUT.PUT_LINE('Sıfıra bölme hatası.');
   WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Başka bir hata oluştu: ' || SQLERRM);
END;
```

### Görev 4: Exception Propagation (İstisna Yayılması)

Bir prosedür oluşturun ve bu prosedür içinde bir istisna tetikleyin. Daha sonra bu prosedürü çağıran bir başka PL/SQL bloğunda bu istisnayı yakalayın.

```plsql
CREATE OR REPLACE PROCEDURE raise_exception IS
BEGIN
   RAISE_APPLICATION_ERROR(-20001, 'Özel hata mesajı.');
END raise_exception;
/

BEGIN
   raise_exception;
EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Hata oluştu: ' || SQLERRM);
END;
```

### Görev 5: İstisna Mesajlarını Loglama

Bir tablo oluşturun ve yakalanan istisnaları bu tabloya kaydedin. Örneğin, hata mesajlarını ve zaman damgasını kaydeden bir tablo oluşturun ve bir PL/SQL bloğu içinde bir hata oluştuğunda bu tabloya bir kayıt ekleyin.

1. **Tablo Oluşturma:**

```plsql
CREATE TABLE error_log (
   error_message VARCHAR2(4000),
   error_time TIMESTAMP
);
```

2. **PL/SQL Bloğu:**

```plsql
DECLARE
   v_number NUMBER;
BEGIN
   v_number := 1 / 0;
EXCEPTION
   WHEN ZERO_DIVIDE THEN
      INSERT INTO error_log (error_message, error_time)
      VALUES ('Sıfıra bölme hatası', SYSTIMESTAMP);
      COMMIT;
END;
```

Bu görevler, PL/SQL exception handling konusundaki becerilerinizi geliştirmek için iyi bir başlangıç olacaktır. Her görevle ilgili kodu çalıştırarak ve çeşitli senaryoları deneyerek daha fazla deneyim kazanabilirsiniz.
