--Loop
--3 nov loop movcuddur:simpe loop,while loop,for in loop
--Simple loop - sert dogru olana kimi dovr eder ve sert dogru olanda dovrden cixar
--Lakin sert olmadiqda sonsuz dovr eder(sonsuz deyerken outputdaki buffer size(karakter sayina kimi davam edecek)
--Eger karakter deyilde insert emelliyati kimi prosesler edende simpe loopdan istifade edib sert vermemisinizse sonsuz sayda
--insert edecek,bu sonsuz program baglanan kimi yada ki siz programi durdurana kimidavam edecek


--simple loop syntax

begin
  loop
   --yazilacaq kodlar;
   end loop;
 end;


SET SERVEROUTPUT ON 
DECLARE 
  v_say number :=0;
BEGIN
  LOOP
   v_say := v_say + 1;
    dbms_output.put_line('Say:'||v_say);
  IF 
    v_say =100 THEN
    EXIT;
  END IF;
    dbms_output.put_line('Proses bitdi');
  END LOOP;
END;


DECLARE 
  v_say number :=0;
BEGIN
  LOOP
   v_say := v_say + 1;
    dbms_output.put_line('Say:'||v_say);
  EXIT WHEN v_say =5;
    dbms_output.put_line('Proses bitdi');
  END LOOP;
END;

--FOR Loop

--for deyisen in baslangic deyeri .. bitis deyeri loop
--yazilacaq kodlar
--end loop;

BEGIN
 FOR i in 10..20 LOOP
   dbms_output.put_line(i);
  END LOOP;
END;

BEGIN
 FOR i in 0..20 LOOP
  IF mod(i,2)=0 THEN
   dbms_output.put_line('cut eded:'||i);
    ELSE 
     dbms_output.put_line('tek eded:'||i);
  END IF;
  END LOOP;
END;
  
--reverse yazarsaq bitis deyerinden baslangic deyerine gedecek
BEGIN
 FOR i in reverse 0..20 LOOP
  IF mod(i,2)=0 THEN
   dbms_output.put_line('cut eded:'||i);
    ELSE 
     dbms_output.put_line('tek eded:'||i);
  END IF;
  END LOOP;
END;


